# etwas weniger Kaffee bitte

  
  
  

## Was wird es geben

Einige kennen es, mit Ansible wurden einige Sachen schön automatisiert, doch die Wartezeiten sind teilweise Lang und der Kaffeekonsum in den Wartezeiten höher als er sein müsste.

  

Allen die etwa aus gesundheitlichen Gründen gern die Wartezeiten reduzieren wollen, um dadurch ihren Kaffeekonsum zu reduzieren, oder auch an diejenigen die ihr Ansible Läufe einfach gern so etwas optimieren wollen oder aber auch an Personen die Überlegen ob sie Ansible für sich einsetzen wollen finden hier weitreichende Informationen zu dem [Vortrag](https://chemnitzer.linux-tage.de/2024/de/programm/beitrag/224) bei [CLT 2024](https://chemnitzer.linux-tage.de/2024/de/) 

 ---

Termin: Sonntag, 11:00 - Raum V1 - Dauer 60 Min.

  

Einige kennen es, mit Ansible wurden einige Sachen schön automatisiert, doch die Wartezeiten sind teilweise Lang und der Kaffeekonsum in den Wartezeiten höher als er sein müsste.

  

Allen die etwa aus gesundheitlichen Gründen gern die Wartezeiten reduzieren wollen, um dadurch ihren Kaffeekonsum zu reduzieren, oder auch an diejenigen die ihr Ansibleläufe einfach gern so etwas optimieren wollen richtet sich der Vortrag.

  

Aber auch an Personen die Überlegen ob sie Ansible für sich einsetzen wollen

  
  

es wird kurz gezeigt wie man die Laufzeiten herrausfinden auch einzelner Tasks.

  

es wird gezeigt welche einfachen/"Standard" Optimierungen zur Verfügung stehen.

Es wird an Beispiel gezeigt was die Optimierungsoption im einzelnen gebracht hat, die Ergebnisse werden präsentiert, da ein Echtzeitlauf den Zeitrahmen sprengen kann

---

Die Präsentation ist [hier](tex/etwas_weniger_kaffee_bitte.pdf)
  

## Erstellen eine virtuellen Umgebung 

erstellen einer Umgebung mit Ansible+ARA+Mitogen
Debian basierte:

    apt install redis
Redhat basiert:

    yum install redis 

Umgebung 

    python3.11 -m venv /opt/ansible/ansible_670  
    source /opt/ansible/ansible_670/bin/activate  
      
    pip install ansible==6.7.0  
    pip install ara[server]  
    pip install mitogen  
    pip install redis



### Links

 - [ARA](https://ara.recordsansible.org/)
 - [Mitogen4Ansible](https://mitogen.networkgenomics.com/ansible_detailed.html)
 - [ansible-parallel](https://git.afpy.org/mdk/ansible-parallel)
 - [ansible-pull](https://docs.ansible.com/ansible/latest/cli/ansible-pull.html)
 - [Semaphore UI](https://www.semui.co/)
	 - [runner](https://docs.semui.co/administration-guide/runners)
 - [Polemarch](https://polemarch.org/)
	 - [cluster](https://about.polemarch.org/en/latest/config.html#polemarch-clustering-overview)
 - [AWX](https://github.com/ansible/awx)
 - [Rundeck](https://www.rundeck.com/)
	 - [Rundeck Cluster](https://resources.rundeck.com/learning/rundeck-cluster-configuration-overview/)

